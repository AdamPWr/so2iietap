CC = g++
CFlags = -std=c++17 -pthread -lncurses -Wno-write-strings
Sources = sources/passenger.cpp main.cpp sources/train.cpp

all: compile

compile:

	$(CC) $(Sources) $(CFlags) -o so2_projekt_etap_2

clear:
	rm *.o clear:
	rm ./heders/*.gch

run: compile
	 ./so2_projekt_etap_2 2> /dev/null
