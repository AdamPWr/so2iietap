#include <iostream>
#include <future>
#include <chrono>
#include <condition_variable>
#include "./headers/passenger.h"
#include "./headers/Soap.h"
#include "./headers/ToiletPaper.h"
#include "./headers/train.h"
#include <vector>
#include "./sources/tui.cpp"

constexpr int NUM_OF_PASSENGERS = 10;

extern constexpr int AMOUNT_OF_TOILET_PAPERS_AND_SOAPS = 5;

int main(int argc, char *argv[])
{

    condition_variable trainDeparture;
    condition_variable trainArrivedAndItIsReadyToGetIt;
    Train train(trainDeparture, trainArrivedAndItIsReadyToGetIt);
    Soap *soap = new Soap[10];
    ToiletPaper *paper = new ToiletPaper[10];

    vector<Passenger *> passengers;

    for (int i = 0; i < NUM_OF_PASSENGERS; i++)
    {
        passengers.push_back(new Passenger(soap, paper, train, trainDeparture, trainArrivedAndItIsReadyToGetIt));
    }

    for (int i = 0; i < NUM_OF_PASSENGERS; i++)
    {
        passengers[i]->run();
    }

    //this_thread::sleep_for(chrono::seconds(600));

    //return 0;

    int frame_num = 0;

    WINDOW *my_win;
    int startx, starty, width, height;
    int ch;

    initscr(); /* Start curses mode 		*/

    TrainVisualisation t;
    PlaceInTrainStation entrance, platform, wc, toilet_paper_supply, soap_supply;

    entrance.position_x = 0;
    entrance.position_y = 0;
    entrance.size_x = 0.5;
    entrance.size_y = 0.4;
    entrance.top_text = "Wejscie na dworzec";
    entrance.bottom_text = "Tutaj pojawiaja sie nowi pasazerowie";
    entrance.busy_slots = 10;

    platform.position_x = 0.0;
    platform.position_y = 0.41;
    platform.size_x = 0.6;
    platform.size_y = 0.4;
    platform.top_text = "Peron dworca";
    platform.bottom_text = "Oczekiwanie na pociag";
    platform.busy_slots = 5;

    wc.position_x = 0.51;
    wc.position_y = 0;
    wc.size_x = 0.49;
    wc.size_y = 0.4;
    wc.top_text = "Lazienka";
    wc.bottom_text = "Tutaj mozna zalatwic swoje potrzeby";
    wc.busy_slots = 3;

    toilet_paper_supply.position_x = 0.62;
    toilet_paper_supply.position_y = 0.41;
    toilet_paper_supply.size_x = 0.2;
    toilet_paper_supply.size_y = 0.35;
    toilet_paper_supply.top_text = "Wolny papier toaletowy";
    toilet_paper_supply.bottom_text = "";
    toilet_paper_supply.busy_slots = 5;

    soap_supply.position_x = 0.84;
    soap_supply.position_y = 0.41;
    soap_supply.size_x = 0.15;
    soap_supply.size_y = 0.35;
    soap_supply.top_text = "Wolne mydlo";
    soap_supply.bottom_text = "";
    soap_supply.busy_slots = 3;

    bool end = false;

    int last_cols = COLS;
    int last_lines = LINES;
    bool resized = true;

    while (!end)
    {

        if (train.isReadyToGetIn == false)
        {
            t.state = TrainVisualisation::State::Departing;
        }
        else
        {
            t.state = TrainVisualisation::State::Staying;
        }

        entrance.busy_slots = 0;
        platform.busy_slots = 0;
        wc.busy_slots = 0;
        toilet_paper_supply.busy_slots = AMOUNT_OF_TOILET_PAPERS_AND_SOAPS;
        soap_supply.busy_slots = AMOUNT_OF_TOILET_PAPERS_AND_SOAPS;

        for (size_t i = 0; i < AMOUNT_OF_TOILET_PAPERS_AND_SOAPS; i++)
        {
            if (!soap[i].available)
            {
                soap_supply.busy_slots--;
            }

            if (!paper[i].available)
            {
                toilet_paper_supply.busy_slots--;
            }
        }

        for (auto &passenger : passengers)
        {

            switch (passenger->passengerState)
            {

            case PASSENGER_STATE::ENTRANCE:
                entrance.busy_slots++;
                // cout << "Entrance" << endl;
                break;

            case PASSENGER_STATE::PLATFORM:
                // cout << "Platform" << endl;
                platform.busy_slots++;
                break;

            case PASSENGER_STATE::TOILET:
                // cout << "Toilet" << endl;
                wc.busy_slots++;
                break;

            default:
                // cout << "Train" << endl;
                break;
            }
        }

        // For some reason windows are not drawn correctly in the first frames
        // So i'm faking the resize event
        if (last_cols != COLS || last_lines != LINES || frame_num < 5)
        {
            resized = true;
        }

        if (resized)
        {
            clear();
        }
        refresh();

        entrance.update(LINES, COLS, resized);
        platform.update(LINES, COLS, resized);
        wc.update(LINES, COLS, resized);
        toilet_paper_supply.update(LINES, COLS, resized);
        soap_supply.update(LINES, COLS, resized);

        t.update(LINES, COLS);

        resized = false;
        last_cols = COLS;
        last_lines = LINES;

        frame_num++;

        timeout(200);
        ch = getch();

        if (ch == 'q')
        {
            end = true;
        }
    }

    endwin(); /* End curses mode		  */

    exit(0);
    return 0;
}
