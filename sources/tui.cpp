#pragma once
#include <ncurses.h>


WINDOW *create_newwin(int height, int width, int starty, int startx)
{
    WINDOW *local_win;

    local_win = newwin(height, width, starty, startx);
    box(local_win, 0, 0); /* 0, 0 gives default characters 
					 * for the vertical and horizontal
					 * lines			*/
    wrefresh(local_win);  /* Show that box 		*/

    return local_win;
}

void destroy_win(WINDOW *local_win)
{
    /* box(local_win, ' ', ' '); : This won't produce the desired
	 * result of erasing the window. It will leave it's four corners
	 * and so an ugly remnant of window.
	 */
    wborder(local_win, ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ');
    /* The parameters taken are
	 * 1. win: the window on which to operate
	 * 2. ls: character to be used for the left side of the window
	 * 3. rs: character to be used for the right side of the window
	 * 4. ts: character to be used for the top side of the window
	 * 5. bs: character to be used for the bottom side of the window
	 * 6. tl: character to be used for the top left corner of the window
	 * 7. tr: character to be used for the top right corner of the window
	 * 8. bl: character to be used for the bottom left corner of the window
	 * 9. br: character to be used for the bottom right corner of the window
	 */
    wrefresh(local_win);
    delwin(local_win);
}

class TrainVisualisation
{
public:
    enum State
    {
        Staying,
        Departing
    };

    State state = State::Staying;
    int position_x = 0;
    int position_y;

    void update(int LINES, int COLS)
    {
        if (state == State::Departing)
        {
            position_x++;
            position_x++;
            position_x++;
        }

        if (state == State::Staying)
        {
            if (position_x != 0)
            {

                position_x = 0;
                // clear();
                // refresh();
            }
        }

        position_y = LINES - 6;

        mvprintw(position_y++, position_x, "       _____                 . . . . . o o o o o                                                                                     ");
        mvprintw(position_y++, position_x, "     __|[_]|__ ___________ _______    ____      o                                                                                    ");
        mvprintw(position_y++, position_x, "    |[] [] []| [] [] [] [] [_____(__  ][]]_n_n__][.                                                                                  ");
        mvprintw(position_y++, position_x, "   _|________|_[_________]_[________]_|__|________)<                                                                                 ");
        mvprintw(position_y++, position_x, "     oo    oo 'oo      oo ' oo    oo 'oo 0000---oo\_                                                                                 ");
    }
};

class PlaceInTrainStation
{

    WINDOW *win;

public:
    int busy_slots = 0;

    float position_x;
    float position_y;
    float size_x;
    float size_y;

    char *top_text;
    char *bottom_text;

    PlaceInTrainStation()
    {
        // Dummy init
        win = create_newwin(0, 0, 0, 0);
    }

    void update(int LINES, int COLS, bool resized)
    {
        if (resized)
        {
            destroy_win(win);
            win = create_newwin(LINES * size_y, COLS * size_x, position_y * LINES, position_x * COLS);
        }

        mvprintw(LINES * position_y + 1, COLS * position_x + 2, top_text);               //2
        mvprintw(LINES * (position_y + size_y) - 2, COLS * position_x + 2, bottom_text); //2

        int slot_count = 0;

        int slot_x;
        int slot_y = LINES * position_y + 3;

        int max_x = COLS * (size_x + position_x);
        int max_y = LINES * (size_y + position_y);

        while (slot_y < max_y - 2)
        {
            slot_x = COLS * position_x + 3;
            while (slot_x < max_x - 5)
            {
                slot_count++ < busy_slots ? mvprintw(slot_y, slot_x, "[X]") : mvprintw(slot_y, slot_x, "[ ]");
                slot_x += 6;
            }

            slot_y += 2;
        }
    }
};
