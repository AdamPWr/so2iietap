#include "../headers/passenger.h"
#include <iostream>
#include <random>

using namespace std;

extern int AMOUNT_OF_TOILET_PAPERS_AND_SOAPS;

Passenger::Passenger(Soap *s, ToiletPaper *p, Train &t, condition_variable &trainDepaturerRequest, condition_variable &p_trainArrivedAndItIsReadyToGetIt) : train(t),
                                                                                                                                                            trainDeparture(trainDepaturerRequest),
                                                                                                                                                            trainArrivedAndItIsReadyToGetIt(p_trainArrivedAndItIsReadyToGetIt)
{
    soap = s;
    paper = p;
    kill_future = kill_signal.get_future();
}

unsigned Passenger::sleepForRandomTime()
{
    std::random_device rd;  //Will be used to obtain a seed for the random number engine
    std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
    std::uniform_real_distribution<> dist(0.5, 4.0);

    default_random_engine generator;
    unsigned sleep_time = dist(gen) * 2000;

    return sleep_time;
}

void Passenger::existance(std::future<void> kill_request)
{
    std::random_device rd;  //Will be used to obtain a seed for the random number engine
    std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
    uniform_int_distribution<int> dist(0, 2);

    int i = 1;
    while (kill_request.wait_for(std::chrono::milliseconds(1)) == std::future_status::timeout)
    {

        if (dist(gen) == 0)
        {
            goToTheToilet();
            useToilet();

            goToTheTrain();
            getIntoTrain();
        }
        else
        {
            goToTheTrain();
            getIntoTrain();
        }

        passengerState = PASSENGER_STATE::ENTRANCE;
    }
}

void Passenger::run()
{
    lifeThread = thread(&Passenger::existance, this, std::move(kill_future));
}

void Passenger::goToTheToilet()
{
    // std::cerr << "Pasazer idzie do wc" << endl;
    this_thread::sleep_for(chrono::milliseconds(sleepForRandomTime()));
    passengerState = PASSENGER_STATE::TOILET;
}

void Passenger::goToTheTrain()
{
    //std::cerr << "Pasazer idzie do pociagu" << endl;
    this_thread::sleep_for(chrono::milliseconds(sleepForRandomTime()));

    passengerState = PASSENGER_STATE::PLATFORM;
}

void Passenger::goToThePlatform()
{
    std::cerr << "Pasazer idzie na peron" << endl;
    this_thread::sleep_for(chrono::milliseconds(sleepForRandomTime()));

    passengerState = PASSENGER_STATE::PLATFORM;
}

void Passenger::useToilet()
{
    std::random_device rd;  
    std::mt19937 gen(rd()); 
    uniform_int_distribution<int> dist(0, 4);

    int index = dist(gen);

    std::scoped_lock<mutex, mutex> scopedLock(soap[index].m, paper[index].m);
    soap[index].available = false;
    paper[index].available = false;
    this_thread::sleep_for(chrono::milliseconds(sleepForRandomTime()));

    soap[index].available = true;
    paper[index].available = true;
}

void Passenger::getIntoTrain()
{
    cerr << "Pasażer próbuje wejśc do pociągu" << endl;
    this_thread::sleep_for(chrono::milliseconds(sleepForRandomTime()));
   \
    bool boarding_train = true;
    while (boarding_train)
    {
        //this_thread::sleep_for(chrono::milliseconds(sleepForRandomTime()));
        if (train.isReadyToGetIn && train.m.try_lock())
        {
            ++train.number_of_passengers;
            passengerState = PASSENGER_STATE::TRAIN;

            std::cerr << "W pociagu mamy " << train.number_of_passengers << " ludzi" << endl;

            if (train.number_of_passengers == 5)
            {
                cerr << "Daje znak do odjazdu" << endl;
                // train.isReadyToGetIn = false;
                trainDeparture.notify_one();
            }
            train.m.unlock(); // To sie moze nie wykonac i bedzie blokada
            boarding_train = false;
        }
    }
    // passenger is waiting for come back from journey
    unique_lock<mutex> lock2(this->m);

    // 1. Zaczekaj aż na trainArrivedAndItIsReadyToGetIt zostanie wywołane notify_all()
    // 2. Sprawdź warunek na końcu
    //  2.1 Jeśli jest prawdziwy - przejdź dalej
    //  2.2 Jeśli jest fałszywy - czekaj dalej
    trainArrivedAndItIsReadyToGetIt.wait(lock2, [&]() {
        if(train.isreturnedFormJourney) cerr<<"Pociag przyjechal z powrotem"<<endl;
        else cerr<<"Jestm w podrozy"<<endl;
        return train.isreturnedFormJourney; });

    {
        --train.number_of_passengers;
        passengerState = PASSENGER_STATE::ENTRANCE;
        std::cerr << "Wrocilem z podrozy! Liczba pasazerow w pociagu " << train.number_of_passengers << endl;
    }
}