#include "../headers/train.h"
#include <iostream>
using namespace std;

Train::Train(condition_variable &p_trainDepaturerRequest, condition_variable &p_trainArrivedAndItIsReadyToGetIt) : trainDeparture(p_trainDepaturerRequest),
                                                                                                                   trainArrivedAndItIsReadyToGetIt(p_trainArrivedAndItIsReadyToGetIt)
{
    kill_future = kill_signal.get_future();
    lifeThread = thread(&Train::existance, this, std::move(kill_future));
}

Train::~Train()
{
    if (lifeThread.joinable())
    {
        lifeThread.join();
    }
}

void Train::departureAndArrive()
{
    isreturnedFormJourney = false; // pociag wyrusza
    isReadyToGetIn = false;
    std::cerr << "Pociag wyrusza" << endl;

    //number_of_passengers = 0;
    std::cerr << "Pociag jedzie " << endl;

    this_thread::sleep_for(chrono::milliseconds(5000)); // pociag jedzie
    isreturnedFormJourney = true;                       // pociag wraca na peron
}

void Train::existance(std::future<void> kill_request)
{
    while (kill_request.wait_for(std::chrono::milliseconds(1)) == std::future_status::timeout)
    {
        unique_lock<mutex> lock(m);
        cerr << "Pociąg czeka ..." << endl;
        trainDeparture.wait(lock, [&]() { return number_of_passengers >= 5; });
        departureAndArrive();
        lock.unlock();
        trainArrivedAndItIsReadyToGetIt.notify_all();
        while (true)
        {
            if (number_of_passengers != 0)
            {
                continue;
            }
            else
            {
                isReadyToGetIn = true;
                break;
            }
        }
        isreturnedFormJourney = false;
        cerr << "Pociag wrocil na stacje" << endl;
    }
}