# Projekt z Systemów Operacyjnych 2

## Stacja kolejowa

### Opis

*Plik screenshot.png zawiera zrzut ekranu - tak program powinien wyglądać po uruchomieniu*

Każdy wątek odpowiada jednemu pasażerowi, który przebywa na stacji i próbuje dostać się do pociągu.
Ze stacji odjeżdza tylko jeden pociag. Na stacji znajduje się też toaleta - aby z niej skorzystać,
pasażer musi zablokować jednocześnie 2 zasoby współdzielone - mydło i papier toaletowy.

Część pasażerów przechodzi wprost na peron, druga część korzysta wpierw z łazienki (prawdopodobieństwo
50:50). Pasażerowie po kolei blokują zasób pociągu i wsiadają do niego pojedynczo. Gdy w pociągu znajdzie się
5 osób, odjedzie on ze stacji. Po odjezdzie pociągu, wątki które w nim jechały, resetowane są do stanu
początkowego - wracają do "głównej hali" dworca, i cykl sie powtarza. Pociąg wraca na peron i jest
gotowy do przyjęcia nowych pasażerów.

### Instrukcja uruchamiania

`make run` - kompiluje i uruchamia program.

Aby zakończyć program, należy nacisnąć `q` na klawiaturze.

**Uwaga**: Wykorzystywałem strumień błędów (cerr), żeby debugować aplikację działającą
w trybie ncurses, dlatego zalecam uruchomienie za pomocą `make run` - w ten sposób,
strumień błędów jest przekierowywany do `/dev/null`. Alternatywnie można oczywiście
wpierw skompilować aplikację poleceniem `make`, a następnie uruchomić ją przez `./so2_projekt_etap_2 2> /dev/null`.