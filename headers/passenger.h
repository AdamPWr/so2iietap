#include <thread>
#include <atomic>
#include <future>
#include <condition_variable>
#include "Soap.h"
#include "ToiletPaper.h"
#include "train.h"

using namespace std;

enum class PASSENGER_STATE
{
    PLATFORM,
    ENTRANCE,
    TOILET,
    TRAIN
};

class Passenger
{
    thread lifeThread;
    Soap *soap; //uskaznik na tablice
    ToiletPaper *paper;
    Train &train;
    future<void> kill_future;
    condition_variable &trainDeparture;
    condition_variable &trainArrivedAndItIsReadyToGetIt;
    mutex m;

public:
    PASSENGER_STATE passengerState = PASSENGER_STATE::ENTRANCE;

    promise<void> kill_signal;
    condition_variable killme;
    Passenger(Soap *s, ToiletPaper *p, Train &t, condition_variable &p_trainDepaturerRequest, condition_variable &p_trainArrivedAndItIsReadyToGetIt);

    ~Passenger()
    {
        if (lifeThread.joinable())
        {
            lifeThread.join();
        }
    }

    void run();
    void existance(std::future<void> kill_request);
    void goToTheToilet();
    void goToTheTrain();
    void goToThePlatform();
    void useToilet();
    void getIntoTrain();
    unsigned sleepForRandomTime();
};
