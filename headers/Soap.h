#pragma once

#include <mutex>
#include <atomic>

class Soap
{
public:
    std::mutex m;
    std::atomic<bool> available = true;
};