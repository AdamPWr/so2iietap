#pragma once

#include <mutex>
#include <atomic>

class ToiletPaper
{
public:
    std::mutex m;
    std::atomic<bool> available = true;
};