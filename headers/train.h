#pragma once
#include<atomic>
#include<mutex>
#include<thread>
#include<future>
#include<condition_variable>

using namespace std;

class Train
{
    public:
    atomic<int> number_of_passengers = 0;
    bool isreturnedFormJourney;
    bool isReadyToGetIn = true;
    mutex m;
    promise<void> kill_signal;
    condition_variable& trainDeparture;
    condition_variable& trainArrivedAndItIsReadyToGetIt;

    Train(condition_variable& p_trainDepaturerRequest, condition_variable& p_trainArrivedAndItIsReadyToGetIt);
    ~Train();
    void departureAndArrive();

    thread lifeThread;
    private:
    future<void> kill_future;

    void existance(std::future<void> kill_request);
};